// import React from 'react';


// let lib = []

// function Book(name, author, pages){
//     this.name = name;
//     this.author = author;
//     this.pages = pages;
// }
// const hp = new Book("half blood prince", "JK", 800)

// function storeText (){
//     bookNames = document.querySelector('.bookName').value 
//     lib.push(bookNames)
//     console.log(lib)
// }


// const Content = () => {



//     return (
//         <div>
        
//         <form onSubmit={storeText} >
//          <label htmlFor="">
//             enter the name of the book
//             <input type="text" className='bookName'/>
//          </label>
//         <button type='submit'>Submit</button>

//         </form>

//         </div>
//     )
// };

// export default Content;

import React, { useState } from 'react';

const UserInputComponent = () => {
  // State to manage the input value and the array of names
  const [inputValue, setInputValue] = useState('');
  const [names, setNames] = useState([]);

  // Function to handle input changes
  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  // Function to handle form submission
  const handleFormSubmit = (event) => {
    event.preventDefault();

    // Check if the input value is not empty
    if (inputValue.trim() !== '') {
      // Update the array of names with the new input value
      setNames((prevNames) => [...prevNames, inputValue]);

      // Clear the input field
      setInputValue('');
    }
  };

  return (
    <div>
      <h1>User Input and Array Example</h1>
      <form onSubmit={handleFormSubmit}>
        <label>
          Enter a name:
          <input type="text" value={inputValue} onChange={handleInputChange} />
        </label>
        <button type="submit">Add Name</button>
      </form>
      
      {/* Display the array of names */}
      <ul>
        {names.map((name, index) => (
          <li key={index}>{name}</li>
        ))}
      </ul>
    </div>
  );
};

export default UserInputComponent;

